from json import loads

from jsonschema import validate
from sample.actual import answer


def test_this():
    schema_file = open(r"schemas/expected.json", encoding="UTF-8")
    schema_str = schema_file.read()
    schema = loads(schema_str)
    schema_file.close()
    validate(answer, schema)
